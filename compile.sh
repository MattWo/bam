#!/bin/sh
CP=conf/:classes/:lib/*
SP=src/java/

/bin/mkdir -p classes/

javac -sourcepath $SP -classpath $CP -d classes/ src/java/bam/*.java src/java/bam/*/*.java || exit 1

/bin/rm -f bam.jar 
jar cf bam.jar -C classes . || exit 1
/bin/rm -rf classes

echo "bam.jar generated successfully"
