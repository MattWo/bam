@ECHO OFF

for %%X in (java.exe) do (set IS_JAVA_IN_PATH=%%~$PATH:X)

IF defined IS_JAVA_IN_PATH (
	start "BAM NRS" java -cp bam.jar;lib\*;conf bam.Bam
) ELSE (
	IF EXIST "%PROGRAMFILES%\Java\jre7" (
		start "BAM NRS" "%PROGRAMFILES%\Java\jre7\bin\java.exe" -cp bam.jar;lib\*;conf bam.Bam
	) ELSE (
		IF EXIST "%PROGRAMFILES(X86)%\Java\jre7" (
			start "BAM NRS" "%PROGRAMFILES(X86)%\Java\jre7\bin\java.exe" -cp bam.jar;lib\*;conf bam.Bam
		) ELSE (
			ECHO Java software not found on your system. Please go to http://java.com/en/ to download a copy of Java.
			PAUSE
		)
	)
)
