package bam.http;

import bam.BamException;
import org.json.simple.JSONStreamAware;

final class ParameterException extends BamException {

    private final JSONStreamAware errorResponse;

    ParameterException(JSONStreamAware errorResponse) {
        this.errorResponse = errorResponse;
    }

    JSONStreamAware getErrorResponse() {
        return errorResponse;
    }

}
