package bam.http;

import bam.Account;
import bam.Alias;
import bam.Asset;
import bam.Generator;
import bam.Bam;
import bam.Order;
import bam.Poll;
import bam.Trade;
import bam.Vote;
import bam.peer.Peer;
import bam.peer.Peers;
import bam.util.Convert;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public final class GetState extends APIServlet.APIRequestHandler {

    static final GetState instance = new GetState();

    private GetState() {
        super(new APITag[] {APITag.INFO});
    }

    @Override
    JSONStreamAware processRequest(HttpServletRequest req) {

        JSONObject response = new JSONObject();

        response.put("application", Bam.APPLICATION);
        response.put("version", Bam.VERSION);
        response.put("time", Convert.getEpochTime());
        response.put("lastBlock", Bam.getBlockchain().getLastBlock().getStringId());
        response.put("cumulativeDifficulty", Bam.getBlockchain().getLastBlock().getCumulativeDifficulty().toString());

        long totalEffectiveBalance = 0;
        for (Account account : Account.getAllAccounts()) {
            long effectiveBalanceBAM = account.getEffectiveBalanceBAM();
            if (effectiveBalanceBAM > 0) {
                totalEffectiveBalance += effectiveBalanceBAM;
            }
        }
        response.put("totalEffectiveBalanceBAM", totalEffectiveBalance);

        response.put("numberOfBlocks", Bam.getBlockchain().getHeight() + 1);
        response.put("numberOfTransactions", Bam.getBlockchain().getTransactionCount());
        response.put("numberOfAccounts", Account.getAllAccounts().size());
        response.put("numberOfAssets", Asset.getAllAssets().size());
        response.put("numberOfOrders", Order.Ask.getAllAskOrders().size() + Order.Bid.getAllBidOrders().size());
        int numberOfTrades = 0;
        for (List<Trade> assetTrades : Trade.getAllTrades()) {
            numberOfTrades += assetTrades.size();
        }
        response.put("numberOfTrades", numberOfTrades);
        response.put("numberOfAliases", Alias.getAllAliases().size());
        response.put("numberOfPolls", Poll.getAllPolls().size());
        response.put("numberOfVotes", Vote.getVotes().size());
        response.put("numberOfPeers", Peers.getAllPeers().size());
        response.put("numberOfUnlockedAccounts", Generator.getAllGenerators().size());
        Peer lastBlockchainFeeder = Bam.getBlockchainProcessor().getLastBlockchainFeeder();
        response.put("lastBlockchainFeeder", lastBlockchainFeeder == null ? null : lastBlockchainFeeder.getAnnouncedAddress());
        response.put("lastBlockchainFeederHeight", Bam.getBlockchainProcessor().getLastBlockchainFeederHeight());
        response.put("isScanning", Bam.getBlockchainProcessor().isScanning());
        response.put("availableProcessors", Runtime.getRuntime().availableProcessors());
        response.put("maxMemory", Runtime.getRuntime().maxMemory());
        response.put("totalMemory", Runtime.getRuntime().totalMemory());
        response.put("freeMemory", Runtime.getRuntime().freeMemory());

        return response;
    }

}
