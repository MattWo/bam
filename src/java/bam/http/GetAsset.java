package bam.http;

import bam.BamException;
import org.json.simple.JSONStreamAware;

import javax.servlet.http.HttpServletRequest;

public final class GetAsset extends APIServlet.APIRequestHandler {

    static final GetAsset instance = new GetAsset();

    private GetAsset() {
        super(new APITag[] {APITag.AE}, "asset");
    }

    @Override
    JSONStreamAware processRequest(HttpServletRequest req) throws BamException {
        return JSONData.asset(ParameterParser.getAsset(req));
    }

}
