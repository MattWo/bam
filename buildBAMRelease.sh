#!/bin/bash

if [ -z "$1" ]; then
    echo "need release tag for checkout"
    exit 1
    else
    RELEASE="$1"
fi

DAY=`date +'%Y%m%d'`
WORKINGDIR=`pwd`"/release/$DAY"

mkdir -p "$WORKINGDIR"
cd "$WORKINGDIR"

git clone https://MattWo@bitbucket.org/MattWo/bam.git
cd bam
git checkout tags/"$RELEASE"

if [ "$?" -ne 0 ]; then
    echo "git  exit with exit code != 0, abort!"
    exit
fi

echo prepairing "$RELEASE"
cd ..

mv bam "bam-$RELEASE"

cd "bam-$RELEASE"

# remove git information
find -name ".git*" -exec rm -f {} \;

./compile.sh
cd ..

#cleanup

rm -rf "bam-$RELEASE"/.git

zip -q -r "bam-$RELEASE".zip "bam-$RELEASE"/

md5sum "bam-$RELEASE".zip > "bam-$RELEASE".zip.md5.txt
sha256sum "bam-$RELEASE".zip > "bam-$RELEASE".zip.sha256.txt
