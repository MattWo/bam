#!/bin/sh
CP=bam.jar:lib/*:conf
SP=src/java/

/bin/rm -rf html/doc/*

javadoc -quiet -sourcepath $SP -classpath $CP -protected -splitindex -subpackages bam -d html/doc/
