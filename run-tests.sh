#!/bin/sh
CP=conf/:classes/:lib/*:testlib/*
SP=src/java/:test/java/
TESTS="bam.crypto.Curve25519Test bam.crypto.ReedSolomonTest bam.peer.HallmarkTest bam.TokenTest"

/bin/mkdir -p classes/

javac -sourcepath $SP -classpath $CP -d classes/ src/java/bam/*.java src/java/bam/*/*.java test/java/bam/*.java test/java/bam/*/*.java || exit 1

java -classpath $CP org.junit.runner.JUnitCore $TESTS

/bin/rm -rf classes

